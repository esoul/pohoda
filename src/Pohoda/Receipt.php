<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda;

use Esoul\Pohoda\Common\AddParameterToHeaderTrait;
use Esoul\Pohoda\Common\OptionsResolver;
use Esoul\Pohoda\Receipt\Header;
use Esoul\Pohoda\Receipt\Item;

class Receipt extends Agenda
{
    use AddParameterToHeaderTrait;

    /** @var string */
    public static $importRoot = 'lst:prijemka';

    /**
     * {@inheritdoc}
     */
    public function __construct(array $data, string $ico, bool $resolveOptions = true)
    {
        // pass to header
        $data = ['header' => new Header($data, $ico, $resolveOptions)];

        parent::__construct($data, $ico, $resolveOptions);
    }

    /**
     * Add item.
     *
     * @param array $data
     *
     * @return $this
     */
    public function addItem(array $data): self
    {
        if (!isset($this->_data['prijemkaDetail'])) {
            $this->_data['prijemkaDetail'] = [];
        }

        $this->_data['prijemkaDetail'][] = new Item($data, $this->_ico);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getXML(): \SimpleXMLElement
    {
        $xml = $this->_createXML()->addChild('pri:prijemka', null, $this->_namespace('pri'));
        $xml->addAttribute('version', '2.0');

        $this->_addElements($xml, ['header', 'prijemkaDetail', 'summary'], 'pri');

        return $xml;
    }

    /**
     * {@inheritdoc}
     */
    protected function _configureOptions(OptionsResolver $resolver)
    {
        // available options
        $resolver->setDefined(['header']);
    }
}
