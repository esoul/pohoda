<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\Stock;

use Esoul\Pohoda\Agenda;
use Esoul\Pohoda\Common\OptionsResolver;

class IntParameter extends Agenda
{
    /**
     * {@inheritdoc}
     */
    public function getXML(): \SimpleXMLElement
    {
        $xml = $this->_createXML()->addChild('stk:intParameter', null, $this->_namespace('stk'));

        $this->_addElements($xml, ['intParameterID', 'intParameterType'], 'stk');

        // value
        $xml->addChild('stk:intParameterValues')->addChild('stk:intParameterValue')->addChild('stk:parameterValue', $this->_data['value']);

        return $xml;
    }

    /**
     * {@inheritdoc}
     */
    protected function _configureOptions(OptionsResolver $resolver)
    {
        // available options
        $resolver->setDefined(['intParameterID', 'intParameterName', 'intParameterOrder', 'intParameterType', 'value']);

        // validate / format options
        $resolver->setRequired('intParameterID');
        $resolver->setNormalizer('intParameterID', $resolver->getNormalizer('int'));
        $resolver->setRequired('intParameterType');
        $resolver->setAllowedValues('intParameterType', ['textValue', 'currencyValue', 'booleanValue', 'numberValue', 'integerValue', 'datetimeValue', 'unit', 'listValue']);
        $resolver->setRequired('value');
    }
}
