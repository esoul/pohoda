<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\Stock;

use Esoul\Pohoda\Agenda;
use Esoul\Pohoda\Common\OptionsResolver;

class Picture extends Agenda
{
    /**
     * {@inheritdoc}
     */
    public function getXML(): \SimpleXMLElement
    {
        $xml = $this->_createXML()->addChild('stk:picture', null, $this->_namespace('stk'));
        $xml->addAttribute('default', $this->_data['default']);

        $this->_addElements($xml, ['filepath', 'description', 'order'], 'stk');

        return $xml;
    }

    /**
     * {@inheritdoc}
     */
    protected function _configureOptions(OptionsResolver $resolver)
    {
        // available options
        $resolver->setDefined(['filepath', 'description', 'order', 'default']);

        // validate / format options
        $resolver->setRequired('filepath');
        $resolver->setNormalizer('order', $resolver->getNormalizer('int'));
        $resolver->setDefault('default', 'false');
        $resolver->setNormalizer('default', $resolver->getNormalizer('bool'));
    }
}
