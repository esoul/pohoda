<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\Stock;

use Esoul\Pohoda\Agenda;
use Esoul\Pohoda\Common\OptionsResolver;

class Category extends Agenda
{
    /**
     * {@inheritdoc}
     */
    public function getXML(): \SimpleXMLElement
    {
        return $this->_createXML()->addChild('stk:idCategory', $this->_data['idCategory'], $this->_namespace('stk'));
    }

    /**
     * {@inheritdoc}
     */
    protected function _configureOptions(OptionsResolver $resolver)
    {
        // available options
        $resolver->setDefined(['idCategory']);

        // validate / format options
        $resolver->setRequired('idCategory');
        $resolver->setNormalizer('idCategory', $resolver->getNormalizer('int'));
    }
}
