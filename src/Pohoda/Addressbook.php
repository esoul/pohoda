<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda;

use Esoul\Pohoda\Addressbook\Header;
use Esoul\Pohoda\Common\AddActionTypeTrait;
use Esoul\Pohoda\Common\AddParameterToHeaderTrait;
use Esoul\Pohoda\Common\OptionsResolver;

class Addressbook extends Agenda
{
    use AddActionTypeTrait;
    use AddParameterToHeaderTrait;

    /** @var string */
    public static $importRoot = 'lAdb:addressbook';

    /**
     * {@inheritdoc}
     */
    public function __construct(array $data, string $ico, bool $resolveOptions = true)
    {
        // pass to header
        if ($data) {
            $data = ['header' => new Header($data, $ico, $resolveOptions)];
        }

        parent::__construct($data, $ico, $resolveOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function getXML(): \SimpleXMLElement
    {
        $xml = $this->_createXML()->addChild('adb:addressbook', null, $this->_namespace('adb'));
        $xml->addAttribute('version', '2.0');

        $this->_addElements($xml, ['actionType', 'header'], 'adb');

        return $xml;
    }

    /**
     * {@inheritdoc}
     */
    protected function _configureOptions(OptionsResolver $resolver)
    {
        // available options
        $resolver->setDefined(['header']);
    }
}
