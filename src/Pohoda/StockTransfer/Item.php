<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\StockTransfer;

use Esoul\Pohoda\Agenda;
use Esoul\Pohoda\Common\OptionsResolver;
use Esoul\Pohoda\Type\StockItem;

class Item extends Agenda
{
    /** @var array */
    protected $_elements = ['quantity', 'stockItem', 'note'];

    /**
     * {@inheritdoc}
     */
    public function __construct(array $data, string $ico, bool $resolveOptions = true)
    {
        // process stock item
        if (isset($data['stockItem'])) {
            $data['stockItem'] = new StockItem($data['stockItem'], $ico, $resolveOptions);
        }

        parent::__construct($data, $ico, $resolveOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function getXML(): \SimpleXMLElement
    {
        $xml = $this->_createXML()->addChild('pre:prevodkaItem', null, $this->_namespace('pre'));

        $this->_addElements($xml, $this->_elements, 'pre');

        return $xml;
    }

    /**
     * {@inheritdoc}
     */
    protected function _configureOptions(OptionsResolver $resolver)
    {
        // available options
        $resolver->setDefined($this->_elements);

        // validate / format options
        $resolver->setNormalizer('quantity', $resolver->getNormalizer('float'));
        $resolver->setNormalizer('note', $resolver->getNormalizer('string90'));
    }
}
