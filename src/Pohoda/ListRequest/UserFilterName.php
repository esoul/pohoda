<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\ListRequest;

use Esoul\Pohoda\Agenda;
use Esoul\Pohoda\Common\OptionsResolver;

class UserFilterName extends Agenda
{
    /**
     * {@inheritdoc}
     */
    public function getXML(): \SimpleXMLElement
    {
        return $this->_createXML()->addChild('ftr:userFilterName', $this->_data['userFilterName'], $this->_namespace('ftr'));
    }

    /**
     * {@inheritdoc}
     */
    protected function _configureOptions(OptionsResolver $resolver)
    {
        // available options
        $resolver->setDefined(['userFilterName']);

        // validate / format options
        $resolver->setRequired('userFilterName');
        $resolver->setNormalizer('userFilterName', $resolver->getNormalizer('string100'));
    }
}
