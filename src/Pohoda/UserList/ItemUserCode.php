<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\UserList;

use Esoul\Pohoda\Agenda;
use Esoul\Pohoda\Common\OptionsResolver;

class ItemUserCode extends Agenda
{
    /**
     * {@inheritdoc}
     */
    public function getXML(): \SimpleXMLElement
    {
        $xml = $this->_createXML()->addChild('lst:itemUserCode', null, $this->_namespace('lst'));
        $xml->addAttribute('code', $this->_data['code']);
        $xml->addAttribute('name', $this->_data['name']);

        if (isset($this->_data['constant'])) {
            $xml->addAttribute('constants', $this->_data['constants']);
        }

        return $xml;
    }

    /**
     * {@inheritdoc}
     */
    protected function _configureOptions(OptionsResolver $resolver)
    {
        // available options
        $resolver->setDefined(['code', 'name', 'constant']);

        // validate / format options
        $resolver->setRequired('code');
        $resolver->setRequired('name');
    }
}
