<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\IntParam;

use Esoul\Pohoda\Agenda;
use Esoul\Pohoda\Common\OptionsResolver;

class Settings extends Agenda
{
    /** @var array */
    protected $_refElements = ['currency'];

    /** @var array */
    protected $_elements = ['unit', 'length', 'currency', 'parameterList'];

    /**
     * {@inheritdoc}
     */
    public function getXML(): \SimpleXMLElement
    {
        $xml = $this->_createXML()->addChild('ipm:parameterSettings', null, $this->_namespace('ipm'));

        $this->_addElements($xml, $this->_elements, 'ipm');

        return $xml;
    }

    /**
     * {@inheritdoc}
     */
    protected function _configureOptions(OptionsResolver $resolver)
    {
        // available options
        $resolver->setDefined($this->_elements);

        // validate / format options
        $resolver->setNormalizer('length', $resolver->getNormalizer('int'));
    }
}
