<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\Common;

trait AddParameterToHeaderTrait
{
    /**
     * Set user-defined parameter.
     *
     * @param string     $name  (can be set without preceding VPr / RefVPr)
     * @param string     $type
     * @param mixed      $value
     * @param mixed|null $list
     *
     * @return \Esoul\Pohoda\Agenda
     */
    public function addParameter(string $name, string $type, $value, $list = null)
    {
        $this->_data['header']->addParameter($name, $type, $value, $list);

        return $this;
    }
}
