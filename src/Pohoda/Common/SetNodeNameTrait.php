<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\Common;

trait SetNodeNameTrait
{
    /** @var string */
    protected $_nodeName;

    /**
     * Set node name.
     *
     * @param string $nodeName
     */
    public function setNodeName(string $nodeName)
    {
        $this->_nodeName = $nodeName;
    }
}
