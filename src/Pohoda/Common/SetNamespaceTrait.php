<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\Common;

trait SetNamespaceTrait
{
    /** @var string */
    protected $_namespace;

    /**
     * Set namespace.
     *
     * @param string $namespace
     */
    public function setNamespace(string $namespace)
    {
        $this->_namespace = $namespace;
    }
}
