<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\Common;

use Esoul\Pohoda\Type\Parameter;

trait AddParameterTrait
{
    /**
     * Set user-defined parameter.
     *
     * @param string     $name  (can be set without preceding VPr / RefVPr)
     * @param string     $type
     * @param mixed      $value
     * @param mixed|null $list
     *
     * @return \Esoul\Pohoda\Agenda
     */
    public function addParameter(string $name, string $type, $value, $list = null)
    {
        if (!isset($this->_data['parameters'])) {
            $this->_data['parameters'] = [];
        }

        $this->_data['parameters'][] = new Parameter([
            'name' => $name,
            'type' => $type,
            'value' => $value,
            'list' => $list
        ], $this->_ico);

        return $this;
    }
}
