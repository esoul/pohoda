<?php
/**
 * This file is part of Esoul/pohoda package.
 *
 * Licensed under the MIT License
 * (c) 
 */

declare(strict_types=1);

namespace Esoul\Pohoda\Common;

use Esoul\Pohoda\Type\ActionType;

trait AddActionTypeTrait
{
    /**
     * Add action type.
     *
     * @param string      $type
     * @param mixed|null  $filter
     * @param string|null $agenda
     *
     * @return self
     */
    public function addActionType(string $type, $filter = null, string $agenda = null): self
    {
        if (isset($this->_data['actionType'])) {
            throw new \OutOfRangeException('Duplicate action type.');
        }

        $this->_data['actionType'] = new ActionType([
            'type' => $type,
            'filter' => $filter,
            'agenda' => $agenda
        ], $this->_ico);

        return $this;
    }
}
